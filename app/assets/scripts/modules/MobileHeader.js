import $ from 'jquery';

class MobileHeader{
    constructor(){
        this.header = $(".header");
        this.menuIcon = $(".header__menu-icon");
        this.menuContainer = $(".header__menu-container");
        this.events();
    }
    
    events(){
        this.menuIcon.click(this.toggleMobileHeader.bind(this));
        //bind method invoking object ke sath bind krne ke lie use hota h agr
        //usme this ke badle Hello pass kiatoo voo Hello display krega
    }
    
    toggleMobileHeader(){
        this.menuContainer.toggleClass("header__menu-container__is-visible");
        this.header.toggleClass("header__is-expanded");
        this.menuIcon.toggleClass("header__menu-icon__close");
    }
}

export default MobileHeader;